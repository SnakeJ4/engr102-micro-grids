from nodes import *
import _json

node = Node(0, 1, {EnergyNode(1, 0, 3), EnergyNode(2, 0, -5), EnergyNode(3, 0, 3), EnergyNode(4, 0, -2)})
for myb_intr in node.calc_instructions():
    if isinstance(myb_intr, Instruction):
        print(str(myb_intr))
