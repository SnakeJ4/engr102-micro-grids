class Node(object):
    def __init__(self, node_id: int, level: int, sub_nodes: set = None):
        self.sub_nodes = sub_nodes
        self.level = level
        self.energy = 0
        self.node_id = node_id
        if sub_nodes is not None:
            for node in sub_nodes:
                self.energy += node.get_energy()

    def get_sub_nodes(self):
        return self.sub_nodes

    def get_energy(self):
        self.energy = 0
        if self.sub_nodes is not None:
            for node in self.sub_nodes:
                self.energy += node.get_energy()

        return self.energy

    def calc_instructions(self):
        # Separate into positive and negative nodes
        positive_nodes = []
        negative_nodes = []
        instruction_list = []
        sub_node_instructions = []
        for node in self.sub_nodes:
            if node.get_energy() > 0:
                positive_nodes.append(node)
            elif node.get_energy() < 0:
                negative_nodes.append(node)

            instruction = node.calc_instructions()
            if instruction is not None:
                sub_node_instructions.append(node.calc_instructions())

        instruction_list.append(sub_node_instructions)
        # Pair most positive nodes with most negative nodes
        positive_nodes.sort(key=lambda pos_node: pos_node.energy, reverse=True)

        negative_nodes.sort(key=lambda neg_node: neg_node.energy)

        # "consume" a node as best as possible before moving to next node
        # every time a balancing act is made add it as an instruction to the set list
        # assume surplus can be provided/taken from outside
        i = 0
        li = -1
        j = 0
        lj = -1
        while i < len(positive_nodes) and j < len(negative_nodes):
            if i != li:
                pos_node: Node = positive_nodes[i]
                eng = pos_node.get_energy()
                li = i

            if j != lj:
                neg_node: Node = negative_nodes[j]
                neg_eng = -neg_node.get_energy()
                lj = j

            if eng >= neg_eng:
                instruction_list.append(Instruction(pos_node, neg_node, neg_eng))
                eng -= neg_eng
                neg_eng = 0
            else:
                instruction_list.append(Instruction(pos_node, neg_node, eng))
                neg_eng -= eng
                eng = 0

            if eng == 0:
                i += 1
            if neg_eng == 0:
                j += 1

        return instruction_list

    def __str__(self):
        return str(self.node_id) + ': ' + str(self.energy)


class EnergyNode(Node):
    def __init__(self, node_id: int, level: int, energy: float):
        super().__init__(node_id, level)
        self.energy = energy

    def get_energy(self):
        return self.energy

    def calc_instructions(self):
        return None

    def set_energy(self, energy: float):
        self.energy = energy


class Instruction(object):
    def __init__(self, source: Node, target: Node, energy: float):
        self.source = source
        self.target = target
        self.energy = energy

    def __str__(self):
        return "Source: " + str(self.source.node_id) + " Target: " + str(self.target.node_id) + " Transfer: " + str(self.energy)
